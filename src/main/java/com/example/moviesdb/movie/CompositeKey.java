package com.example.moviesdb.movie;


import jakarta.persistence.*;

import java.io.Serializable;

@Embeddable
public class CompositeKey implements Serializable {


    @Column(name = "email")
    private String email;

    @Column(name = "imdbID")
    private String imdbID;

    public CompositeKey() {

    }
    public CompositeKey(String email, String imdbID) {
        this.email = email;
        this.imdbID = imdbID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }


    @Override
    public String toString() {
        return "CompositeKey{" +
                ", email='" + email + '\'' +
                ", imdbID='" + imdbID + '\'' +
                '}';
    }
}
