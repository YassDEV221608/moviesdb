package com.example.moviesdb.movie;

import com.example.moviesdb.movie.Movie;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MovieRepository extends JpaRepository<Movie,CompositeKey> {
    boolean existsByCompositeKeyEmailAndCompositeKeyImdbID(String email,String imdbID);

    @Transactional
    void deleteByCompositeKeyEmailAndCompositeKeyImdbID(String email,String imdbID);

    List<Movie> findByCompositeKeyEmail(String email);
}
