package com.example.moviesdb.movie;


import jakarta.persistence.*;

@Entity
public class Movie {

    @EmbeddedId
    private CompositeKey compositeKey;
    private String poster;
    private String year;
    private String type;
    private String title;
    public Movie() {

    }

    public Movie(CompositeKey compositeKey, String poster, String year, String type, String title) {
        this.compositeKey = compositeKey;
        this.poster = poster;
        this.year = year;
        this.type = type;
        this.title = title;
    }

    public CompositeKey getCompositeKey() {
        return compositeKey;
    }

    public void setCompositeKey(CompositeKey compositeKey) {
        this.compositeKey = compositeKey;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Movie{" +
                ", compositeKey=" + compositeKey +
                ", poster='" + poster + '\'' +
                ", year='" + year + '\'' +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
