package com.example.moviesdb.movie;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://ec2-16-171-40-49.eu-north-1.compute.amazonaws.com:4200/","http://16.171.40.49:4200/","http://localhost:4200/"})
@RestController
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;

    @GetMapping("/exists/{email}/{imdbID}")
    public ResponseEntity<String> exists(@PathVariable("email") String email,@PathVariable("imdbID") String imdbID) {
        if(movieRepository.existsByCompositeKeyEmailAndCompositeKeyImdbID(email,imdbID)) {
            return ResponseEntity.ok().body("{\"exists\" : \"true\"}");
        }
        return  ResponseEntity.status(404).body("{\"exists\" : \"false\"}");
    }


    @PostMapping(value = "/addFavorite")
    public ResponseEntity<String> addMovie(@RequestBody Movie movie) {
        if(!movieRepository.existsByCompositeKeyEmailAndCompositeKeyImdbID(movie.getCompositeKey().getEmail(),movie.getCompositeKey().getImdbID())){
            movieRepository.save(movie);
            return ResponseEntity.ok().body("{\"message\": \"Added to favorite\"}");
        }
        return  ResponseEntity.status(404).body("{\"message\": \"Already exist\"}");
    }

    @DeleteMapping("/removeFavorite/{email}/{imdbID}")
    public ResponseEntity<String> removeMovie(@PathVariable("email") String email,@PathVariable("imdbID") String imdbID) {
        if(movieRepository.existsByCompositeKeyEmailAndCompositeKeyImdbID(email,imdbID)){
            movieRepository.deleteByCompositeKeyEmailAndCompositeKeyImdbID(email,imdbID);
            return ResponseEntity.ok().body("{\"message\": \"Removed from to favorite\"}");
        }
        return  ResponseEntity.status(404).body("{\"message\": \"Do not exist\"}");
    }

    @GetMapping("/getFavorites/{email}")
    public List<Movie> getFavorites(@PathVariable("email") String email) {
        return movieRepository.findByCompositeKeyEmail(email);
    }
}
