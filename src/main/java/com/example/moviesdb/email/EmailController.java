package com.example.moviesdb.email;


import com.example.moviesdb.user.User;
import com.example.moviesdb.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin(origins = {"http://ec2-16-171-40-49.eu-north-1.compute.amazonaws.com:4200/","http://16.171.40.49:4200/","http://localhost:4200/"})
public class EmailController {
    @Autowired
    private UserRepository userRepository;


    @GetMapping("/verify/{token}")
    public ResponseEntity<String> verifyEmail(@PathVariable("token") String verificationToken) {
        User user = userRepository.findByToken(verificationToken);
        if(user != null) {
            user.setVerified(true);
            userRepository.save(user);
            return ResponseEntity.ok().body("{\"message\": \"Your email address has been verified\"}");
        }
        return ResponseEntity.status(404).body("{\"error\": \"User not found or verification token is invalid\"}");
    }
}
