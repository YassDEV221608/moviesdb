package com.example.moviesdb.test;

import com.example.moviesdb.movie.CompositeKey;
import com.example.moviesdb.movie.Movie;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
public class MovieTest {

    @Test
    void testMovieConstructorAndGetters() {
        CompositeKey compositeKey = new CompositeKey("test@example.com", "tt0111161");
        Movie movie = new Movie(compositeKey, "posterUrl", "1994", "movie", "The Shawshank Redemption");

        assertEquals(compositeKey, movie.getCompositeKey());
        assertEquals("posterUrl", movie.getPoster());
        assertEquals("1994", movie.getYear());
        assertEquals("movie", movie.getType());
        assertEquals("The Shawshank Redemption", movie.getTitle());
    }

    @Test
    void testMovieSetters() {
        CompositeKey compositeKey = new CompositeKey("test@example.com", "tt0111161");
        Movie movie = new Movie();

        movie.setCompositeKey(compositeKey);
        movie.setPoster("posterUrl");
        movie.setYear("1994");
        movie.setType("movie");
        movie.setTitle("The Shawshank Redemption");

        assertEquals(compositeKey, movie.getCompositeKey());
        assertEquals("posterUrl", movie.getPoster());
        assertEquals("1994", movie.getYear());
        assertEquals("movie", movie.getType());
        assertEquals("The Shawshank Redemption", movie.getTitle());
    }

    @Test
    void testDefaultConstructor() {
        Movie movie = new Movie();

        assertNull(movie.getCompositeKey());
        assertNull(movie.getPoster());
        assertNull(movie.getYear());
        assertNull(movie.getType());
        assertNull(movie.getTitle());
    }

    @Test
    void testToString() {
        CompositeKey compositeKey = new CompositeKey("test@example.com", "tt0111161");
        Movie movie = new Movie(compositeKey, "posterUrl", "1994", "movie", "The Shawshank Redemption");

        String expectedString = "Movie{" +
                ", compositeKey=" + compositeKey +
                ", poster='posterUrl'" +
                ", year='1994'" +
                ", type='movie'" +
                ", title='The Shawshank Redemption'" +
                '}';

        assertEquals(expectedString, movie.toString());
    }
}
