package com.example.moviesdb.comment;


import jakarta.persistence.*;

@Entity
public class Comment {
    @SequenceGenerator(name="comment_id_seq",sequenceName = "comment_id_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comment_id_seq")
    @Id
    private Long id;
    private String email;
    private String firstname;
    private String lastname;
    private String imdbID;
    private String date;
    private String body;

    public Comment() {

    }

    public Comment(String email, String firstname, String lastname, String imdbID, String date,String body) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.imdbID = imdbID;
        this.date = date;
        this.body = body;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "email='" + email + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", imdbID='" + imdbID + '\'' +
                ", date='" + date + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
