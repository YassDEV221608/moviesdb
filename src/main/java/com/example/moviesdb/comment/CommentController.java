package com.example.moviesdb.comment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://ec2-16-171-40-49.eu-north-1.compute.amazonaws.com:4200/","http://16.171.40.49:4200/","http://localhost:4200/"})
public class CommentController {
    @Autowired
    private CommentRepository commentRepository;

    @PostMapping("saveComment")
    public ResponseEntity<String> saveComment(@RequestBody Comment comment) {
        commentRepository.save(comment);
        return ResponseEntity.ok().body("comment saved");
    }

    @GetMapping("getComments/{imdbID}")
    public List<Comment> getComments(@PathVariable("imdbID") String imdbID) {
        List<Comment> comments = commentRepository.findAllByImdbID(imdbID);
        return comments;
    }


}
